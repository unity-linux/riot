# SPEC file overview:
# https://docs.fedoraproject.org/en-US/quick-docs/creating-rpm-packages/#con_rpm-spec-file-overview
# Fedora packaging guidelines:
# https://docs.fedoraproject.org/en-US/packaging-guidelines/
%global _build_id_links none
%define debug_package %{nil}
%define vermajor 1.1
%define verminor 1

Name:		riot
Version:	%{vermajor}.%{verminor}
Release:	1%{?dist}
Summary:	A decentralized, secure messaging client for collaborative group communication

License:	ASL 2.0
URL:		https://riot.im/
Source: https://github.com/taw00/riot-rpm/blob/master/SOURCES/riot-web-%{vermajor}.%{verminor}.tar.gz?raw=true
Source1: https://github.com/taw00/riot-rpm/blob/master/SOURCES/%{name}-%{vermajor}-contrib.tar.gz?raw=true1

BuildRequires: desktop-file-utils
BuildRequires: libappstream-glib
BuildRequires: nodejs nodejs-yarn npm

Provides: riot-web = 0.9.6
Obsoletes: riot-web < 0.9.6
ExclusiveArch: x86_64 i686 i586 i386

%description
Riot is a decentralized, secure messaging client for collaborative group
communication. Riot's core architecture is an implementation of the matrix
protocol.

Riot is more than a messaging app. Riot is a shared work-space for the web.
Riot is a place to connect with teams. Riot is a place to to collaborate, to
work, to discuss your current projects.

Riot removes the barriers between apps, allowing you to connect teams and
functionality like never before.

Riot is free. Riot is secure.

%prep
mkdir %{name}-%{vermajor}
%setup -T -D -n %{name}-%{vermajor} -a0
%setup -T -D -n %{name}-%{vermajor} -a1

# Make sure the right library path is used...
echo "%{_libdir}/%{name}" > %{name}-%{vermajor}-contrib/etc-ld.so.conf.d_%{name}.conf

%build
cd riot-web-%{vermajor}.%{verminor}
echo "\
# yarn alias inserted here by the Riot RPM specfile build script
# this can be removed after build is complete
# nodejs-yarn installs /usr/bin/yarnpkg for some reason (conflicts?). So, we
# simply alias it so that embedded scripts don't stumble over this anomaly
alias yarn='/usr/bin/yarnpkg'" >> ~/.bashrc
source ~/.bashrc
yarn install
yarn build

%ifarch x86_64 amd64
  %define linuxunpacked electron_app/dist/linux-unpacked
  ./node_modules/.bin/build -l tar.gz --x64
%else
  %define linuxunpacked electron_app/dist/linux-ia32-unpacked
  ./node_modules/.bin/build -l tar.gz --ia32
%endif

%install
# Create directories
install -d %{buildroot}%{_libdir}/%{name}
install -d -m755 -p %{buildroot}%{_bindir}
install -d %{buildroot}%{_datadir}/%{name}
install -d %{buildroot}%{_datadir}/applications
install -d %{buildroot}%{_sysconfdir}/ld.so.conf.d
%define _metainfodir %{_datadir}/metainfo

cp -a riot-web-%{vermajor}.%{verminor}/%{linuxunpacked}/* %{buildroot}%{_datadir}/%{name}
# bug https://github.com/vector-im/riot-web/issues/9166 ... alerted by user "Aaron"
install -D -m644 -p riot-web-%{vermajor}.%{verminor}/config.sample.json %{buildroot}%{_datadir}/%{name}/resources/webapp/config.json

# a little ugly - symbolic link creation
ln -s %{_datadir}/%{name}/riot-web %{buildroot}%{_bindir}/%{name}

declare -a SizeArray=("16x16" "22x22" "24x24" "32x32" "48x48" "128x128" "256x256")
for size in "${SizeArray[@]}"; do
    install -d -p %{buildroot}%{_datadir}/icons/hicolor/"${size}"/apps/
    install -d -p %{buildroot}%{_datadir}/icons/HighContrast/"${size}"/apps/
    install -D -m644 -p %{name}-%{vermajor}-contrib/desktop/riot.hicolor."${size}".png   %{buildroot}%{_datadir}/icons/hicolor/"${size}"/apps/riot.png
    install -D -m644 -p %{name}-%{vermajor}-contrib/desktop/riot.highcontrast."${size}".png   %{buildroot}%{_datadir}/icons/HighContrast/"${size}"/apps/riot.png
done

install -d -p %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/
install -d -p %{buildroot}%{_datadir}/icons/HighContrast/scalable/apps/
install -D -m644 -p %{name}-%{vermajor}-contrib/desktop/riot.hicolor.svg         %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/riot.svg
install -D -m644 -p %{name}-%{vermajor}-contrib/desktop/riot.highcontrast.svg         %{buildroot}%{_datadir}/icons/HighContrast/scalable/apps/riot.svg

install -m755  %{name}-%{vermajor}-contrib/desktop/riot.wrapper.sh %{buildroot}%{_bindir}/
install -D -m644 -p %{name}-%{vermajor}-contrib/desktop/riot.desktop %{buildroot}%{_datadir}/applications/riot.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/riot.desktop
install -D -m644 -p %{name}-%{vermajor}-contrib/desktop/riot.appdata.xml %{buildroot}%{_metainfodir}/riot.appdata.xml
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.appdata.xml

# /usr/lib/riot or /usr/lib64/riot...
install -D -m755 -p %{buildroot}%{_datadir}/%{name}/libffmpeg.so %{buildroot}%{_libdir}/%{name}/libffmpeg.so
rm %{buildroot}%{_datadir}/%{name}/libffmpeg.so
install -D -m644 -p %{name}-%{vermajor}-contrib/etc-ld.so.conf.d_riot.conf %{buildroot}%{_sysconfdir}/ld.so.conf.d/riot.conf

%post
umask 007
/sbin/ldconfig > /dev/null 2>&1
/usr/bin/update-desktop-database &> /dev/null || :

%postun
umask 007
/sbin/ldconfig > /dev/null 2>&1
/usr/bin/update-desktop-database &> /dev/null || :

%files
%defattr(-,root,root,-)
%license riot-web-%{vermajor}.%{verminor}/LICENSE
# We own /usr/share/riot and everything under it...
%{_datadir}/%{name}
%{_datadir}/icons/*
%{_datadir}/applications/riot.desktop
%{_metainfodir}/riot.appdata.xml
%{_bindir}/*
%{_sysconfdir}/ld.so.conf.d/riot.conf
%dir %attr(755,root,root) %{_libdir}/%{name}
%{_libdir}/%{name}/libffmpeg.so

%changelog
